# ImPoolProblem
This problem relates to the following [issue](https://github.com/mosra/magnum-integration/issues/94).

My compile command:
```
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE="C:\dev\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Ninja" -DVCPKG_TARGET_TRIPLET=x64-windows & ninja
```

Results in error messages of the sort:
```
implot.cpp.obj : error LNK2019: unresolved external symbol "__declspec(dllimport) public: void __cdecl ImPool<struct ImPlotPlot>::Clear(void)" (__imp_?Clear@?$ImPool@UImPlotPlot@@@@QEAAXXZ) referenced in function "void __cdecl ImPlot::BustPlotCache(void)" (?BustPlotCache@ImPlot@@YAXXZ)
```
It can be circumvented by changing imgui/imgui_internal.h from `struct IMGUI_API ImPool` to `struct __declspec( dllexport ) ImPool`, which suggests that IMGUI_API is not set correctly for this file.

